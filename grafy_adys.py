# -*- coding: utf-8 -*-
"""
Created on Sun Dec  7 14:58:18 2014

@author: Ada
"""
# Przykładowe wywołania: ####################################
# dla 'u.data': aa = show_recomendations(4,5,6,5,6)
# dla 'u.data': aa = show_recomendations(30,50,100,943,1682)
#############################################################

import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import sys



def generuj_graf(tabInc, kroki):
    
    ###### GRAFY ########################################################
    
    #max grubość krawędzi grafu
    maxedgewidth=15
    
    #tablica z danymy wejściowymi
    #     f1 f2 f3 . fn
    #    ______________
    # u1 |x  x  x  .  x
    # u2 |x  x  x  .  x
    # u3 |x  x  x  .  x
    # .  |.  .  .  .  .
    # un |x  x  x  x  x
     
    #konwersja do tablicy (problem przy przetwarzaniu macierzy)
    input_table = np.squeeze(np.asarray(tabInc))
    
    #tworzenie listy krawędzi (z atrybutem)
    combs = []
    it = np.nditer(input_table,flags=['multi_index'])
    while not it.finished:    
        if int(it[0]) != 0:
            temp=it.multi_index
            temp=temp+(int(it[0]),)
            combs.append(temp)
        it.iternext()
    
    for i,e in enumerate(combs): 
        temp=list(combs[i])
        temp[0]='u'+str(temp[0])    
        temp[1]='f'+str(temp[1])
        combs[i]=tuple(temp)
    
    #tworzenie wierzchołków - użytkownicy (w pionie)
    nodes_u=[]
    for i in range(len(input_table)):
        if i > 0:
            nodes_u.append('u'+str(i))
        
    #tworzenie wierzchołków - filmy (w poziomie)
    nodes_f=[]
    for j in range(len(input_table[0])):
        if j > 0:
            nodes_f.append('f'+str(j)) 
    
    
    #tworznie grafu
    G = nx.Graph()
    
    G.add_nodes_from(nodes_u, group='users')
    G.add_nodes_from(nodes_f, group='films')
    G.add_weighted_edges_from(combs)
          
    
    #szukanie największej wartości (potrzebne do skalowania)               
    maxwidth=np.amax(np.array([(d['weight'])
        for u,v,d in G.edges(data=True)]))
            
    # wartości wag do wyświetlenia na etykietach (rzeczywiste)
    edgelabels=dict([((u,v,),d['weight'])
        for u,v,d in G.edges(data=True)])
    
    # wartości wag do narysowania połączeń (przeskalowane)
    edgewidth=[]
    for n,nbrs in G.adjacency_iter():
        for nbr,eattr in nbrs.items():
            data=eattr['weight']/maxwidth*maxedgewidth
            edgewidth.append(data) 
            
            
    ### layout - pozycja węzłów #################################
                               
              
    #tworzenie pierwszej współrzędnej dla węzłów-filmów        
    fst = np.arange(len(nodes_f))/10 
    #wyśrodkowanie
    off = fst[len(fst)-1]/2
    fst = fst-off
    
    sec=np.ones(len(nodes_f))*0.5 #wysokość węzłów filmów
    tuplets_f=(zip(fst,sec))
    
    pos_f=dict(zip(nodes_f,tuplets_f))
    
    #tworzenie pierwszej współrzędnej dla węzłów-użytkowników    
    fst_2 = np.arange(len(nodes_u))/10 
    #wyśrodkowanie
    off_2 = fst_2[len(fst_2)-1]/2
    fst_2 = fst_2-off_2
    
    sec_2=np.ones(len(nodes_u))*0.2 #wysokość węzłów użytkowników
    tuplets_u=(zip(fst_2,sec_2))
    
    pos_u=dict(zip(nodes_u,tuplets_u))
    
    #pos = dict(list(pos_u.items()) + list(pos_f.items()))    
    pos = nx.circular_layout(G)
    
    
    #pos = nx.graphviz_layout(G)
    # rysowanie
    newbbox = dict(alpha=0)
    
    nx.draw_networkx_nodes(G,pos,nodelist=nodes_f,node_color='g',node_size=500)                      
    nx.draw_networkx_nodes(G,pos,nodelist=nodes_u,node_color='r',node_size=500)
    nx.draw_networkx_labels(G,pos,fontsize=14)                    
    nx.draw_networkx_edges(G,pos, width=edgewidth)                
    nx.draw_networkx_edge_labels(G,pos,edge_labels=edgelabels,label_pos=0.8,rotate=True,font_size=12,font_color='DarkGoldenrod',font_weight='bold',bbox=newbbox)
    
    plt.axis('off')
    #plt.figure(kroki)
    plt.savefig("graf_"+str(kroki)+".png")
    
    print("Zapisano graf_"+str(kroki)+".png")
    plt.close()
    
    return
#koniec funkcji    
    
    
    
    
    
###### DANE #############################################
#tablica z danymy wejściowymi
#     f1 f2 f3 . fn
#    ______________
# u1 |x  x  x  .  x
# u2 |x  x  x  .  x
# u3 |x  x  x  .  x
# .  |.  .  .  .  .
# un |x  x  x  x  x

#def grafy(levels, all_users, all_films):
#    
#    #tworzenie pustej macierzy 943_users X 1682_items
#    tableInc = np.zeros((all_users+1,all_films+1))
#    
#    #uzupełnienie macierzy danymi z ml100k
#    __file__ = 'u.data'
#    
#    try:
#        f = open(__file__, "r")
#        try:
#            for line in f:
#                user, film, mark, data = line.split()
#                tableInc[int(user)][int(film)] = int(mark)-3
#                
#        finally:
#            f.close()
#            
#    except (IOError, OSError):
#        print >> sys.stderr, "File can not be read."
#        sys.exit(1)
#        
#    #liczba użytkowników i filmów    
#    users=len(tableInc)
#    films=len(tableInc[0]) 
# 
#    macierz = np.zeros((users+films)**2).reshape(users+films,users+films)
#    #macierz = np.arange((users+films)**2).reshape(users+films,users+films)
#    macierz[films:, :films] = tableInc
#    macierz[:films, films:] = np.transpose(tableInc)
#    macierz = np.matrix(macierz,np.int32)
#    n=1
#    for i in np.arange(1,levels*2,2):
#        m_macierz = macierz**i
#        inputtab=m_macierz[films:, :films]
#        generuj_graf(inputtab,n)
#        print("Cykl "+str(n) +", potęga:"+str(i))
#        n=n+1
#    return
    
##############################################################
# oblicz ntą (level) macierz z rekomendacjami    
def matrix_pow(level,tableInc):

    #liczba użytkowników i filmów    
    users=len(tableInc)
    films=len(tableInc[0]) 
    
    macierz = np.zeros((users+films)**2).reshape(users+films,users+films)
    #macierz = np.arange((users+films)**2).reshape(users+films,users+films)
    macierz[films:, :films] = tableInc
    macierz[:films, films:] = np.transpose(tableInc)
    macierz = np.matrix(macierz,np.int32)
    n=1
    for i in np.arange(1,level*2,2):
        m_macierz = macierz**i
        inputtab=m_macierz[films:, :films]
        #generuj_graf(inputtab,n)
        print("Cykl "+str(n) +", potęga:"+str(i))
        #print(inputtab)
        n=n+1
    return inputtab
    
    
############################################################################# 
# funkcja tworzy pustą macierz, ładuje w nią dane z ml100k 
# i wybiera zbiór treningowy 
#####################################################################      

def find_new_matrix(level, dim_user, dim_film,all_users, all_films):
    #tworzenie pustej macierzy 943_users X 1682_items
    entire_matrix = np.zeros((all_users+1,all_films+1), dtype=int)

    #macierz wypełniona jedynkami, posłuży do sprawdzenia czy użytkownik już
    #obejrzał film
    mask_matrix = np.zeros((all_users+1,all_films+1), dtype=int)
    
    #uzupełnienie macierzy danymi z ml100k
    __file__ = 'u.data'
    
    try:
        f = open(__file__, "r")
        try:
            for line in f:
                user, film, mark, data = line.split()
                entire_matrix[int(user)][int(film)] = int(mark)-3
                if int(mark) > 0:
                    mask_matrix[int(user)][int(film)] = 1
                
#                jeśli użytkownik ocenił film na >3, to uznaję że go lubi 
#                if int(mark) > 3:
#                    entire_matrix[int(user)][int(film)] = 1 #int(mark)
        finally:
            f.close()
            
    except (IOError, OSError):
        print >> sys.stderr, "File can not be read."
        sys.exit(1)
        
    #wybór zbioru treningowego
    _users = dim_user+1
    _films = dim_film+1
    part_matrix = entire_matrix[:_users, :_films]

    
    
    # train_mask zawiera maskę z losowo rozłożonymi jedynkami
    # - służy do 'przysłonienia' połowy macierzy   
    train_mask = np.random.randint(2, size=(_users, _films))

    # - do testowania rekomendacji   
    mask_unwatched = mask_matrix[:_users, :_films]
    
    # test_mask zawiera maskę odwrotną do train_mask 
    test_mask = train_mask*(-1)+1
    help_mask = train_mask*mask_unwatched
    help_mask_inv = help_mask*(-1)+1

    
    
    #macierz, która będzie potęgowana
    train_part_matrix = part_matrix * train_mask
   
    #macierz po potęgowaniu
    powered_matrix = matrix_pow(level,train_part_matrix)
     
    
    tpm = np.copy(train_part_matrix)     
    for x in (tpm):
        for index, item in enumerate(x):
            if item > 0:
                x[index] = 1
    tpm = tpm * (-1) + 1
    output_matrix = np.asarray(powered_matrix)*mask_unwatched
##### wizualizacja tylko małych zbiorow:

    if dim_user < 10: 
            
        generuj_graf(train_part_matrix,1)
        generuj_graf(powered_matrix,level) 
        print ('---------cały-------------------------------')  
        print (np.asarray(part_matrix))
        print ('---------tren maska-------------------------')  
        print (np.asarray(train_mask))
        print ('---------trenowany--------------------------')  
        print (np.asarray(train_part_matrix))
        print ('---------dopełnienie trenpwanego------------')  
        print (np.asarray(tpm))
        print ('---------spotęgowany------------------------')  
        print (powered_matrix)
        print ('--spotęgowany*dopełnienie trenpwanego-------')
        print (np.asarray(powered_matrix)*tpm) 
        print ('--------------------------------------------')    
    
    output_matrix = np.asarray(powered_matrix)*tpm    
    
    return output_matrix, train_mask, help_mask_inv,test_mask
    
############################################################################# 
# funkcja    
    
def find_recomendations(level, dim_user, dim_film, all_users, all_films):
    combs = []
    calc_matrix = find_new_matrix(level, dim_user, dim_film,all_users, all_films)
    
    maxim = np.max(calc_matrix[0])
    
    final_matrix = calc_matrix[0]
    combs = []
    
    it = np.nditer(final_matrix,flags=['multi_index'])
    while not it.finished: 
        row = [] 
        temp=it.multi_index
        temp=temp+(int(it[0]),)
        row.append(temp)
        
        if dim_user<10:
            if temp[2] > 0: #> maxim/2
                combs.append([temp[0], temp[1]])
        else:
######### ogranicz liczbę rekomendacji
            if temp[2] >  2/3*maxim: #ogranicz liczbę rekomendacji
                combs.append([temp[0], temp[1]])
                
                
        it.iternext()
                
        
    return combs, calc_matrix[2],calc_matrix[3]     
    
############################################################################# 
# funkcja
    
def show_recomendations(level, dim_user, dim_film, all_users, all_films): 
    counted = find_recomendations(level, dim_user, dim_film, all_users, all_films)  
     
    
    from collections import defaultdict
    by_userid = defaultdict(list)
    for (u_id, f_id) in counted[0]:
        by_userid[u_id].append((f_id))

    __file__ = 'u.data'
    
    try:
        f = open(__file__, "r")
        base_matrix = np.zeros((all_users+1,all_films+1))
        try:
            for line in f:
                user, film, mark, data = line.split()                
                base_matrix[int(user)][int(film)] = int(mark)
                
        finally:
            f.close()
            
    except (IOError, OSError):
        print >> sys.stderr, "File can not be read."
        sys.exit(1)   
        
    small_base_matrix = base_matrix[:dim_user+1, :dim_film+1]
    small_base_matrix = small_base_matrix# * counted[1]* counted[2]
    
    base_by_userid = []
    
    it = np.nditer(small_base_matrix,flags=['multi_index'])
    while not it.finished: 
        row = [] 
        temp=it.multi_index
        temp=temp+(int(it[0]),)
        row.append(temp)
        if temp[2] > 0:
            base_by_userid.append([temp[0], (temp[1],temp[2])])        
        it.iternext()                
    
        
    real_matrix = defaultdict(list)
    for (u,f) in base_by_userid:
        real_matrix[u].append((f))
    
    key_list = []
    for key, val in by_userid.items():
       key_list.append(key)

       
    marks_list =[]
    for key, val in real_matrix.items():
        if key in key_list:
            marks_list.append([key,val])
    marks_dict = dict((x[0], (x[1])) for x in marks_list)
    
    print(by_userid)        
    return  by_userid, marks_dict

     
            
                
            

    

